#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include <ctime>
#include <random>
#include <cassert>
#include <cstdint>
#include <limits>
#include <tuple>
#include <iomanip>
#include "lodepng.h"
#include "ThreadPool.h"

#define FOREACH_NEIGHBORS_OF(x, y) \
    for (int dy = -1; dy <= 1; dy++) { \
        if (y + dy < 0 || y + dy >= png.height) continue; \
        for (int dx = -1; dx <= 1; dx++) { \
            if (!dy && !dx) continue; \
            if (x + dx < 0 || x + dx >= png.width) continue; \
            int qx = x + dx; \
            int qy = y + dy;
#define END_FOREACH_NEIGHBORS }}
#define ERROR(msg) { std::cerr << msg << "\n"; exit(1); }

const int INF = std::numeric_limits<int>::max();
const char* helpStr =
"Usage: acimg [-n 32] [-p 5] [-d m]\n"
"\n"
"   -n [16|32|64|128|256] (default: 32)\n"
"        16 -->   64 x   64 image (~instant)\n"
"        32 -->  256 x  128 image (~seconds)\n"
"        64 -->  512 x  512 image (~minutes)\n"
"       128 --> 2048 x 1024 image (~hours)\n"
"       256 --> 4096 x 4096 image (~days)\n"
"\n"
"   -p [1-9] (default: 5)\n"
"       Starting point\n"
"         1   2   3\n"
"         4   5   6\n"
"         7   8   9\n"
"\n"
"   -d [m|a] (default: m)\n"
"       Strategy to calculate point-color difference\n"
"         m = min (fast but grainy)\n"
"         a = avg (slow)\n"
"\n"
"   -s [0-4294967295] (default: time(0))\n"
"       Seed\n"
"\n"
"   -h  Help: print this message";

struct Color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
    Color(uint8_t r=0, uint8_t g=0, uint8_t b=0, uint8_t a=255) :
        r{r}, g{g}, b{b}, a{a} { }
};
struct Point {
    int x, y;
    Point(int x=0, int y=0) : x{x}, y{y} {}
    struct Hash { size_t operator()(const Point& p)const { return p.x^p.y; } };
    bool operator==(const Point& p)const { return x == p.x && y == p.y; }
};
struct RawImage {
    int width;
    int height;
    std::vector<uint8_t> data;
    RawImage(int w, int h) : width{w}, height{h}, data(4*w*h, 0) {}
    void setPixel(int x, int y, Color c)
    {
        uint8_t* ptr = &data[4*width*y + 4*x];
        ptr[0] = c.r;
        ptr[1] = c.g;
        ptr[2] = c.b;
        ptr[3] = c.a;
    }
    Color getPixel(int x, int y)const
    {
        const uint8_t* ptr = &data[4*width*y + 4*x];
        return {ptr[0], ptr[1], ptr[2], ptr[3]};
    }
};
using PointVec = std::vector<Point>;
typedef int (*CalcDiff)(const RawImage&, int, int, Color);
using ColorVec = std::vector<Color>;
using PointHSet = std::unordered_set<Point, Point::Hash>;

int colorDiff(Color, Color);
PointVec getNeighbors(const Point&, int, int);
int calcMinDiff(const RawImage&, int, int, Color);
int calcAvgDiff(const RawImage&, int, int, Color);
RawImage generateImage(unsigned, int, int, int, int, int, CalcDiff calcDiff);
void parseArgs(int, char**, int&, int&, int&, int&, int&, CalcDiff&,
               unsigned&, std::string&);

int main(int argc, char** argv)
{
    int numColors = 32;
    int width  = 256;
    int height = 128;
    int startX = width / 2;
    int startY = height / 2;
    CalcDiff calcDiff = calcMinDiff;
    unsigned seed = time(nullptr);
    std::string filename;

    parseArgs(argc, argv, numColors, width, height, startX, startY, calcDiff,
              seed, filename);

    auto start = clock();
    auto image = generateImage(seed, numColors, width, height, startX, startY,
                               calcDiff);
    lodepng::encode(filename.c_str(), image.data, width, height);
    auto end = clock();
    std::cout << double(end - start) / CLOCKS_PER_SEC << "s\n";
}


int colorDiff(Color lhs, Color rhs)
{
    int r = lhs.r - rhs.r;
    int g = lhs.g - rhs.g;
    int b = lhs.b - rhs.b;
    return r*r + g*g + b*b;
}

int calcMinDiff(const RawImage& png, int x, int y, Color c)
{
    int ret = INF;
    FOREACH_NEIGHBORS_OF(x, y)
        if (png.getPixel(qx, qy).a)
            ret = std::min(ret, colorDiff(c, png.getPixel(qx, qy)));
    END_FOREACH_NEIGHBORS
    return ret;
}

int calcAvgDiff(const RawImage& png, int x, int y, Color c)
{
    int ret = 0;
    int count = 0;
    FOREACH_NEIGHBORS_OF(x, y)
        if (png.getPixel(qx, qy).a)
        {
            ret += colorDiff(c, png.getPixel(qx, qy));
            count++;
        }
    END_FOREACH_NEIGHBORS
    return ret / count;
}

using AvailCIt = PointHSet::const_iterator;

std::tuple<Point, int>
getBestPoint(AvailCIt beg, AvailCIt end, const RawImage& png, Color color,
             CalcDiff calcDiff)
{
    Point best;
    int minW = INF;
    while (beg != end)
    {
        Point p = *beg;
        int w = calcDiff(png, p.x, p.y, color);
        if (w < minW)
        {
            minW = w;
            best = p;
        }
        ++beg;
    }
    return {best, minW};
}

void insertTransparentPixelPoint(PointHSet& avail,
                                 int x, int y, const RawImage& png)
{
    FOREACH_NEIGHBORS_OF(x, y)
        if (!png.getPixel(qx, qy).a)
            avail.emplace(qx, qy);
    END_FOREACH_NEIGHBORS
}

RawImage generateImage(unsigned seed, int numColors, int width, int height,
                      int startX, int startY, CalcDiff cd)
{
    assert(numColors * numColors * numColors == width * height);

    RawImage  png(width, height);
    ColorVec  colors;
    PointHSet available;
    ThreadPool pool(3);

    // Shuffle all colors
    colors.reserve(numColors * numColors * numColors);
    for (int r = 0; r < numColors; r++)
        for (int g = 0; g < numColors; g++)
            for (int b = 0; b < numColors; b++)
                colors.emplace_back(r * 255 / (numColors - 1),
                                    g * 255 / (numColors - 1),
                                    b * 255 / (numColors - 1));
    std::mt19937 prng;
    prng.seed(seed);
    std::shuffle(begin(colors), end(colors), prng);

    // Starting point
    Point best = Point(startX, startY);
    png.setPixel(best.x, best.y, colors[0]);
    insertTransparentPixelPoint(available, best.x, best.y, png);

    int n4KBPix = width * height / 4096;
    std::cout << "0/" << n4KBPix; std::cout.flush();

    for (size_t i = 1; i < colors.size(); ++i)
    {
        auto cl = colors[i];
        // Find best match point for color `cl` in `available`
        auto it0 = begin(available);
        auto it1 = it0;
        std::advance(it1, available.size() / 4);
        auto it2 = it1;
        std::advance(it2, available.size() / 4);
        auto it3 = it2;
        std::advance(it3, available.size() / 4);
        auto it4 = end(available);
        auto t2 = pool.enqueue(getBestPoint, it1, it2, std::cref(png), cl, cd);
        auto t3 = pool.enqueue(getBestPoint, it2, it3, std::cref(png), cl, cd);
        auto t4 = pool.enqueue(getBestPoint, it3, it4, std::cref(png), cl, cd);
        auto [best1, min1] = getBestPoint(it0, it1, png, cl, cd);
        auto [best2, min2] = t2.get();
        auto [best3, min3] = t3.get();
        auto [best4, min4] = t4.get();
        if (min2 < min1) { best1 = best2; min1 = min2; }
        if (min4 < min3) { best3 = best4; min3 = min4; }
        best = min3 < min1 ? best3 : best1;

        // put the pixel where it belongs
        png.setPixel(best.x, best.y, cl);
        // adjust the available list
        available.erase(best);
        insertTransparentPixelPoint(available, best.x, best.y, png);

        if ((i & 4095) == 0)
        {
            std::cout << "\r" << (i >> 12) << "/" << n4KBPix
                      << " " << std::setw(8) << available.size();
            std::cout.flush();
        }
    }
    std::cout << "\r" << n4KBPix << "/" << n4KBPix
              << " " << std::setw(8) << available.size() << "\n";
    std::cout.flush();
    return png;
}

unsigned stou(const std::string& str, size_t* idx=nullptr, int base=10)
{
    unsigned long result = std::stoul(str, idx, base);
    if (result > std::numeric_limits<unsigned>::max())
        throw std::out_of_range("stou");
    return result;
}

void parseArgs(int argc, char** argv, int& numColors, int& width, int& height,
               int& startX, int& startY, CalcDiff& calcDiff, unsigned& seed,
               std::string& filename)
{
    static Point sz[] = {
        {64, 64}, {256, 128}, {512, 512}, {2048, 1024}, {4096, 4096}
    };

    int c = 'm';
    int d = 5;
    int dx = 1;
    int dy = 1;
    for (int i = 1; i < argc; ++i)
    {
        const char* s = argv[i];
        if (s[0] == '-') //command
        {
            if (s[1] == 'h')
            {
                std::cout << helpStr << "\n";
                exit(0);
            }
            else if (s[1] == 'n')
            {
                if (++i == argc)
                    ERROR("Error while parsing `numColors`");
                try {
                    numColors = std::stoi(argv[i]);
                } catch (const std::exception&) {
                    ERROR("Error while parsing `numColors`");
                }
                unsigned szid = 0;
                for (auto n = numColors; n; n>>=1) szid++;
                szid -= 5;
                if (szid >= sizeof(sz)/sizeof(*sz))
                    ERROR("Error while parsing `width` and `height`");
                width  = sz[szid].x;
                height = sz[szid].y;
            }
            else if (s[1] == 'p')
            {
                if (++i == argc)
                    ERROR("Error while parsing starting position");
                try {
                    d = std::stoi(argv[i]);
                } catch (const std::exception&) {
                    ERROR("Error while parsing starting position");
                }
                if (d < 1 || d > 9)
                    ERROR("Error while parsing starting position");
                dx = (d - 1) % 3;
                dy = (d - 1) / 3;
            }
            else if (s[1] == 'd')
            {
                if (++i == argc)
                    ERROR("Error while parsing strategy");
                c = argv[i][0];
                if (c == 'm')
                    calcDiff = calcMinDiff;
                else if (c == 'a')
                    calcDiff = calcAvgDiff;
                else
                    ERROR("Error while parsing strategy");
            }
            else if (s[1] == 's')
            {
                if (++i == argc)
                    ERROR("Error while parsing `seed`");
                try {
                    seed = stou(argv[i]);
                } catch (const std::exception&) {
                    ERROR("Error while parsing `seed`");
                }
            }
        }
    }
    startX = width / 2 * dx;
    startY = height / 2 * dy;
    if (startX == width)  --startX;
    if (startY == height) --startY;
    filename = std::string(1, c) + "-" + std::to_string(d) + "-" +
               std::to_string(numColors) + "-" + std::to_string(seed) + ".png";
}
